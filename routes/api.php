<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('add/new/ingredient', 'IngredientController@addNewIngredient');
Route::get('get/all/ingredients', 'IngredientController@getAllIngredients')->middleware('token');
Route::post('add/new/recipe', 'RecipeController@addNewRecipe');
Route::get('get/all/recipes', 'RecipeController@getAllRecipes');
Route::post('add/new/order', 'OrderController@addNewOrder')->middleware(['token', 'date']);
Route::get('get/all/orders/info', 'OrderController@getAllOrdersInfo');
Route::delete('delete/recipe/{recipeId}', 'RecipeController@deleteRecipe');
Route::patch('update/recipe', 'RecipeController@updateRecipe');
Route::get('get/recipe/by/id/{recipeId}', 'RecipeController@getRecipeById');
