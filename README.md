# README #


### Install and run the project:###

 
* $ cd recipeProject 
* $ composer install

### DB Configuration:###

* DB_CONNECTION=mysql
* DB_HOST=127.0.0.1
* DB_DATABASE=recipe
* DB_PORT=3306
* DB_USERNAME=db-user-name
* DB_PASSWORD=db-user-pass


### Run laravel api ###

* $ php artisan migrate
* $ php artisan serve

### DB seeder ###

* $ php artisan db:seed --class= SEEDER_NAME => to run specific seeder
* $ php artisan db:seed --class= SEEDER_NAME => to run all seeders

### PHPUnit test ###
* composer test

### Run Vue Front-end ###
* $ cd /path/to/front-end
* $ npm install
* $ npm run serve