-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: localhost    Database: recipe
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredient`
--

DROP TABLE IF EXISTS `ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredient` (
  `in_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `in_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `in_measure_id` int(10) DEFAULT NULL,
  `in_supplier` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`in_id`),
  KEY `in_measure_fk_idx` (`in_measure_id`),
  CONSTRAINT `in_measure_id_fk` FOREIGN KEY (`in_measure_id`) REFERENCES `measure` (`m_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredient`
--

LOCK TABLES `ingredient` WRITE;
/*!40000 ALTER TABLE `ingredient` DISABLE KEYS */;
INSERT INTO `ingredient` VALUES (1,'Ing1',1,'S1','2021-02-03 12:16:17','2021-02-03 12:16:17'),(5,'Ing2',2,'S2','2021-02-03 12:17:28','2021-02-03 12:17:28'),(6,'Ing3',3,'S3','2021-02-03 12:17:28','2021-02-03 12:17:28');
/*!40000 ALTER TABLE `ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `ingredient_view`
--

DROP TABLE IF EXISTS `ingredient_view`;
/*!50001 DROP VIEW IF EXISTS `ingredient_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `ingredient_view` AS SELECT 
 1 AS `in_id`,
 1 AS `in_name`,
 1 AS `in_measure_id`,
 1 AS `in_supplier`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `measure`
--

DROP TABLE IF EXISTS `measure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `measure` (
  `m_id` int(10) NOT NULL AUTO_INCREMENT,
  `m_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`m_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measure`
--

LOCK TABLES `measure` WRITE;
/*!40000 ALTER TABLE `measure` DISABLE KEYS */;
INSERT INTO `measure` VALUES (1,'kg','2021-02-03 12:11:43','2021-02-03 12:11:43'),(2,'g','2021-02-03 12:17:29','2021-02-03 12:17:29'),(3,'pieces','2021-02-03 12:17:29','2021-02-03 12:17:29');
/*!40000 ALTER TABLE `measure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `measure_view`
--

DROP TABLE IF EXISTS `measure_view`;
/*!50001 DROP VIEW IF EXISTS `measure_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `measure_view` AS SELECT 
 1 AS `m_id`,
 1 AS `m_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_100000_create_password_resets_table',1),(2,'2019_08_19_000000_create_failed_jobs_table',1),(3,'2021_02_02_220309_ingredient',1),(4,'2021_02_02_220622_order',1),(5,'2021_02_02_220648_recipe',1),(6,'2021_02_02_220720_recipe_ingredients',1),(7,'2021_02_02_220755_user',1),(8,'2021_02_03_115309_measure',2),(9,'2021_02_03_141852_test_view',3),(10,'2021_02_03_142238_test_view',4),(11,'2021_02_03_142459_ingredient_view',5),(12,'2021_02_03_142531_measure_view',5),(13,'2021_02_03_142558_order_info_view',5),(14,'2021_02_03_142607_order_view',5),(15,'2021_02_03_142634_recipe_info_view',5),(16,'2021_02_03_142701_recipe_ingredients_view',5),(17,'2021_02_03_142714_recipe_view',5),(18,'2021_02_03_142721_user_view',5);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `o_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `o_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `o_recipe_id` bigint(20) DEFAULT NULL,
  `o_user_id` bigint(20) DEFAULT NULL,
  `o_delivery_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`o_id`),
  KEY `o_user_id_fk_idx` (`o_user_id`),
  KEY `o_recipe_id_fk_idx` (`o_recipe_id`),
  CONSTRAINT `o_recipe_id_fk` FOREIGN KEY (`o_recipe_id`) REFERENCES `recipe` (`r_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `o_user_id_fk` FOREIGN KEY (`o_user_id`) REFERENCES `user` (`u_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `order_info_view`
--

DROP TABLE IF EXISTS `order_info_view`;
/*!50001 DROP VIEW IF EXISTS `order_info_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `order_info_view` AS SELECT 
 1 AS `r_id`,
 1 AS `r_name`,
 1 AS `r_description`,
 1 AS `ri_id`,
 1 AS `ri_amount`,
 1 AS `in_id`,
 1 AS `in_name`,
 1 AS `in_supplier`,
 1 AS `m_id`,
 1 AS `m_name`,
 1 AS `o_id`,
 1 AS `o_date`,
 1 AS `o_delivery_date`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `order_view`
--

DROP TABLE IF EXISTS `order_view`;
/*!50001 DROP VIEW IF EXISTS `order_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `order_view` AS SELECT 
 1 AS `o_id`,
 1 AS `o_date`,
 1 AS `o_recipe_id`,
 1 AS `o_user_id`,
 1 AS `o_delivery_date`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipe`
--

DROP TABLE IF EXISTS `recipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recipe` (
  `r_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `r_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`r_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe`
--

LOCK TABLES `recipe` WRITE;
/*!40000 ALTER TABLE `recipe` DISABLE KEYS */;
INSERT INTO `recipe` VALUES (1,'srt','dffd','2020-12-08 13:14:26','2020-12-08 13:14:26'),(2,'r2','d2','2020-12-08 13:14:26','2020-12-08 13:14:26'),(3,'r3','d3','2020-12-08 13:14:26','2020-12-08 13:14:26');
/*!40000 ALTER TABLE `recipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `recipe_info_view`
--

DROP TABLE IF EXISTS `recipe_info_view`;
/*!50001 DROP VIEW IF EXISTS `recipe_info_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `recipe_info_view` AS SELECT 
 1 AS `r_id`,
 1 AS `r_name`,
 1 AS `r_description`,
 1 AS `ri_id`,
 1 AS `ri_amount`,
 1 AS `in_id`,
 1 AS `in_name`,
 1 AS `in_supplier`,
 1 AS `m_id`,
 1 AS `m_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `recipe_ingredients`
--

DROP TABLE IF EXISTS `recipe_ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recipe_ingredients` (
  `ri_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ri_recipe_id` bigint(20) DEFAULT NULL,
  `ri_ingredient_id` bigint(20) DEFAULT NULL,
  `ri_amount` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ri_id`),
  KEY `ri_recipe_id_fk_idx` (`ri_recipe_id`),
  KEY `ri_ingredient_id_fk_idx` (`ri_ingredient_id`),
  CONSTRAINT `ri_ingredient_id_fk` FOREIGN KEY (`ri_ingredient_id`) REFERENCES `ingredient` (`in_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ri_recipe_id_fk` FOREIGN KEY (`ri_recipe_id`) REFERENCES `recipe` (`r_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe_ingredients`
--

LOCK TABLES `recipe_ingredients` WRITE;
/*!40000 ALTER TABLE `recipe_ingredients` DISABLE KEYS */;
INSERT INTO `recipe_ingredients` VALUES (3,2,5,3,'2020-12-08 13:14:26','2020-12-08 13:14:26'),(40,1,5,22.5,'2021-02-06 03:23:02','2021-02-06 03:23:02'),(41,1,1,21.5,'2021-02-06 03:23:02','2021-02-06 03:23:02');
/*!40000 ALTER TABLE `recipe_ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `recipe_ingredients_view`
--

DROP TABLE IF EXISTS `recipe_ingredients_view`;
/*!50001 DROP VIEW IF EXISTS `recipe_ingredients_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `recipe_ingredients_view` AS SELECT 
 1 AS `ri_id`,
 1 AS `ri_recipe_id`,
 1 AS `ri_ingredient_id`,
 1 AS `ri_amount`,
 1 AS `in_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `recipe_view`
--

DROP TABLE IF EXISTS `recipe_view`;
/*!50001 DROP VIEW IF EXISTS `recipe_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `recipe_view` AS SELECT 
 1 AS `r_id`,
 1 AS `r_name`,
 1 AS `r_description`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `u_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `u_email` varchar(100) DEFAULT NULL,
  `u_first_name` varchar(50) DEFAULT NULL,
  `u_second_name` varchar(50) DEFAULT NULL,
  `u_last_name` varchar(50) DEFAULT NULL,
  `u_token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `u_token_UNIQUE` (`u_token`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'u1@com','User1','etr','hjk','f18712ec4a49da7bec009f296946435f','2021-02-03 11:15:58','2021-02-03 11:15:58'),(2,'u2@com','User2','er','etgf','7ebc1535b67ab05d56f4babece65685d','2021-02-03 11:16:59','2021-02-03 11:16:59'),(3,'u3@com','User3','era','xghg','7520ea9a140a5c41e5ca963178f51f1e','2021-02-03 11:18:08','2021-02-03 11:18:08'),(4,'u4@com','User4','wer','jyu','2910ef45088a5a7b859f85bb11cfa365','2021-02-03 11:21:47','2021-02-03 11:21:47'),(5,'u5@com','User5','qw','df','1cc4e1452e134424d127263000d69f9b','2021-02-03 12:17:28','2021-02-03 12:17:28');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `user_view`
--

DROP TABLE IF EXISTS `user_view`;
/*!50001 DROP VIEW IF EXISTS `user_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `user_view` AS SELECT 
 1 AS `u_id`,
 1 AS `u_email`,
 1 AS `u_first_name`,
 1 AS `u_second_name`,
 1 AS `u_last_name`,
 1 AS `u_token`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `ingredient_view`
--

/*!50001 DROP VIEW IF EXISTS `ingredient_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ingredient_view` AS select `ingredient`.`in_id` AS `in_id`,`ingredient`.`in_name` AS `in_name`,`ingredient`.`in_measure_id` AS `in_measure_id`,`ingredient`.`in_supplier` AS `in_supplier` from `ingredient` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `measure_view`
--

/*!50001 DROP VIEW IF EXISTS `measure_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `measure_view` AS select `measure`.`m_id` AS `m_id`,`measure`.`m_name` AS `m_name` from `measure` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `order_info_view`
--

/*!50001 DROP VIEW IF EXISTS `order_info_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `order_info_view` AS select `recipe`.`r_id` AS `r_id`,`recipe`.`r_name` AS `r_name`,`recipe`.`r_description` AS `r_description`,`recipe_ingredients`.`ri_id` AS `ri_id`,`recipe_ingredients`.`ri_amount` AS `ri_amount`,`ingredient`.`in_id` AS `in_id`,`ingredient`.`in_name` AS `in_name`,`ingredient`.`in_supplier` AS `in_supplier`,`measure`.`m_id` AS `m_id`,`measure`.`m_name` AS `m_name`,`order`.`o_id` AS `o_id`,`order`.`o_date` AS `o_date`,`order`.`o_delivery_date` AS `o_delivery_date` from ((((`recipe` join `recipe_ingredients` on((`recipe`.`r_id` = `recipe_ingredients`.`ri_recipe_id`))) join `ingredient` on((`recipe_ingredients`.`ri_ingredient_id` = `ingredient`.`in_id`))) join `measure` on((`measure`.`m_id` = `ingredient`.`in_measure_id`))) join `order` on((`order`.`o_recipe_id` = `recipe`.`r_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `order_view`
--

/*!50001 DROP VIEW IF EXISTS `order_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `order_view` AS select `order`.`o_id` AS `o_id`,`order`.`o_date` AS `o_date`,`order`.`o_recipe_id` AS `o_recipe_id`,`order`.`o_user_id` AS `o_user_id`,`order`.`o_delivery_date` AS `o_delivery_date` from `order` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `recipe_info_view`
--

/*!50001 DROP VIEW IF EXISTS `recipe_info_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `recipe_info_view` AS select `recipe`.`r_id` AS `r_id`,`recipe`.`r_name` AS `r_name`,`recipe`.`r_description` AS `r_description`,`recipe_ingredients`.`ri_id` AS `ri_id`,`recipe_ingredients`.`ri_amount` AS `ri_amount`,`ingredient`.`in_id` AS `in_id`,`ingredient`.`in_name` AS `in_name`,`ingredient`.`in_supplier` AS `in_supplier`,`measure`.`m_id` AS `m_id`,`measure`.`m_name` AS `m_name` from (((`recipe` join `recipe_ingredients` on((`recipe`.`r_id` = `recipe_ingredients`.`ri_recipe_id`))) join `ingredient` on((`recipe_ingredients`.`ri_ingredient_id` = `ingredient`.`in_id`))) join `measure` on((`measure`.`m_id` = `ingredient`.`in_measure_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `recipe_ingredients_view`
--

/*!50001 DROP VIEW IF EXISTS `recipe_ingredients_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `recipe_ingredients_view` AS select `recipe_ingredients`.`ri_id` AS `ri_id`,`recipe_ingredients`.`ri_recipe_id` AS `ri_recipe_id`,`recipe_ingredients`.`ri_ingredient_id` AS `ri_ingredient_id`,`recipe_ingredients`.`ri_amount` AS `ri_amount`,`ingredient_view`.`in_name` AS `in_name` from (`recipe_ingredients` join `ingredient_view` on((`recipe_ingredients`.`ri_ingredient_id` = `ingredient_view`.`in_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `recipe_view`
--

/*!50001 DROP VIEW IF EXISTS `recipe_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `recipe_view` AS select `recipe`.`r_id` AS `r_id`,`recipe`.`r_name` AS `r_name`,`recipe`.`r_description` AS `r_description` from `recipe` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_view`
--

/*!50001 DROP VIEW IF EXISTS `user_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `user_view` AS select `user`.`u_id` AS `u_id`,`user`.`u_email` AS `u_email`,`user`.`u_first_name` AS `u_first_name`,`user`.`u_second_name` AS `u_second_name`,`user`.`u_last_name` AS `u_last_name`,`user`.`u_token` AS `u_token` from `user` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-06  5:38:27
