<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RecipeIngredients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_ingredients', function (Blueprint $table) {
            $table->bigIncrements('ri_id')->unsigned(false);
            $table->bigInteger('ri_recipe_id')->nullable();
            // $table->foreign('ri_recipe_id')->references('r_id')->on('recipe');
            $table->bigInteger('ri_ingredient_id')->nullable();
            // $table->foreign('ri_ingredient_id')->references('in_id')->on('ingredient');
            $table->double('ri_amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe_ingredients');
    }
}
