<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("

        CREATE 
        ALGORITHM = UNDEFINED 
        DEFINER = `root`@`localhost` 
        SQL SECURITY DEFINER
    VIEW `recipe`.`order_view` AS
        SELECT 
            `recipe`.`order`.`o_id` AS `o_id`,
            `recipe`.`order`.`o_date` AS `o_date`,
            `recipe`.`order`.`o_user_id` AS `o_user_id`,
            `recipe`.`order`.`o_recipe_id` AS `o_recipe_id`,
            `recipe`.`order`.`o_delivery_date` AS `o_delivery_date`
        FROM
            `recipe`.`order`

        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
