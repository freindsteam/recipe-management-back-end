<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class User extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->bigIncrements('u_id')->unsigned(false);
            $table->string('u_email', 100)->nullable();
            $table->string('u_first_name', 50)->nullable();
            $table->string('u_second_name', 50)->nullable();
            $table->string('u_last_name', 50)->nullable();
            $table->string('u_token', 255)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
