<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("

        CREATE 
        ALGORITHM = UNDEFINED 
        DEFINER = `root`@`localhost` 
        SQL SECURITY DEFINER
    VIEW `recipe`.`user_view` AS
        SELECT 
            `recipe`.`user`.`u_id` AS `u_id`,
            `recipe`.`user`.`u_email` AS `u_email`,
            `recipe`.`user`.`u_first_name` AS `u_first_name`,
            `recipe`.`user`.`u_second_name` AS `u_second_name`,
            `recipe`.`user`.`u_last_name` AS `u_last_name`,
            `recipe`.`user`.`u_token` AS `u_token`
        FROM
            `recipe`.`user`

        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
