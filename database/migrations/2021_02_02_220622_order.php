<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class Order extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->bigIncrements('o_id')->unsigned(false);
            $table->timestamp('o_date')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->bigInteger('o_user_id')->nullable();
            //$table->foreign('o_user_id')->references('u_id')->on('user');
            $table->bigInteger('o_recipe_id')->nullable();
            //$table->foreign('o_recipe_id')->references('r_id')->on('recipe');
            $table->date('o_delivery_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
