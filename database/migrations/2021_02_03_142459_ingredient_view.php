<?php

use Illuminate\Database\Migrations\Migration;

class IngredientView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("

        CREATE
        ALGORITHM = UNDEFINED
        DEFINER = `root`@`localhost`
        SQL SECURITY DEFINER
    VIEW `recipe`.`ingredient_view` AS
        SELECT
            `recipe`.`ingredient`.`in_id` AS `in_id`,
            `recipe`.`ingredient`.`in_name` AS `in_name`,
            `recipe`.`ingredient`.`in_measure_id` AS `in_measure_id`,
            `recipe`.`ingredient`.`in_supplier` AS `in_supplier`
        FROM
            `recipe`.`ingredient`

        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
