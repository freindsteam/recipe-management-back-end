<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ingredient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredient', function (Blueprint $table) {
            $table->bigIncrements('in_id')->unsigned(false);
            $table->string('in_name', 50)->nullable();
            $table->integer('in_measure_id')->nullable();
            //$table->foreign('in_measure_id')->references('m_id')->on('measure');
            $table->string('in_supplier', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredient');
    }
}
