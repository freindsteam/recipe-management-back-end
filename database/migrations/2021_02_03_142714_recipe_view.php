<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RecipeView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("

        CREATE 
        ALGORITHM = UNDEFINED 
        DEFINER = `root`@`localhost` 
        SQL SECURITY DEFINER
    VIEW `recipe`.`recipe_view` AS
        SELECT 
            `recipe`.`recipe`.`r_id` AS `r_id`,
            `recipe`.`recipe`.`r_name` AS `r_name`,
            `recipe`.`recipe`.`r_description` AS `r_description`
        FROM
            `recipe`.`recipe`

        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
