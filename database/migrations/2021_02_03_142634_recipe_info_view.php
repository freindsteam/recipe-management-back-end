<?php

use Illuminate\Database\Migrations\Migration;

class RecipeInfoView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("

        CREATE
        ALGORITHM = UNDEFINED
        DEFINER = `root`@`localhost`
        SQL SECURITY DEFINER
    VIEW `recipe`.`recipe_info_view` AS
        SELECT
            `recipe`.`recipe`.`r_id` AS `r_id`,
            `recipe`.`recipe`.`r_name` AS `r_name`,
            `recipe`.`recipe`.`r_description` AS `r_description`,
            `recipe`.`recipe_ingredients`.`ri_id` AS `ri_id`,
            `recipe`.`recipe_ingredients`.`ri_amount` AS `ri_amount`,
            `recipe`.`ingredient`.`in_id` AS `in_id`,
            `recipe`.`ingredient`.`in_name` AS `in_name`,
            `recipe`.`ingredient`.`in_supplier` AS `in_supplier`,
            `recipe`.`measure`.`m_id` AS `m_id`,
            `recipe`.`measure`.`m_name` AS `m_name`
        FROM
            (((`recipe`.`recipe`
            JOIN `recipe`.`recipe_ingredients` ON ((`recipe`.`recipe`.`r_id` = `recipe`.`recipe_ingredients`.`ri_recipe_id`)))
            JOIN `recipe`.`ingredient` ON ((`recipe`.`recipe_ingredients`.`ri_ingredient_id` = `recipe`.`ingredient`.`in_id`)))
            JOIN `recipe`.`measure` ON ((`recipe`.`measure`.`m_id` = `recipe`.`ingredient`.`in_measure_id`)))

        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
