<?php

use Illuminate\Database\Migrations\Migration;

class RecipeIngredientsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("

        CREATE
        ALGORITHM = UNDEFINED
        DEFINER = `root`@`localhost`
        SQL SECURITY DEFINER
    VIEW `recipe`.`recipe_ingredients_view` AS
        SELECT
            `recipe`.`recipe_ingredients`.`ri_id` AS `ri_id`,
            `recipe`.`recipe_ingredients`.`ri_recipe_id` AS `ri_recipe_id`,
            `recipe`.`recipe_ingredients`.`ri_ingredient_id` AS `ri_ingredient_id`,
            `recipe`.`recipe_ingredients`.`ri_amount` AS `ri_amount`
        FROM
            `recipe`.`recipe_ingredients`

        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
