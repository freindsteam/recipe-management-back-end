<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateMeasureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('measure')->insert([
            [
                'm_name' => 'kg',
            ],
            [
                'm_name' => 'g',
            ],
            [
                'm_name' => 'piece',
            ],
        ]);
    }
}
