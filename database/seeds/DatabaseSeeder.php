<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CreateUserSeeder::class);
        $this->call(CreateMeasureSeeder::class);
        $this->call(CreateIngredientSeeder::class);
        $this->call(CreateRecipeSeeder::class);
        $this->call(CreateOrderSeeder::class);

    }
}
