<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateIngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ingredient')->insert([
            [
                'in_name' => 'Ing1',
                'in_measure_id' => 1,
                'in_supplier' => 'S1',
            ],
            [
                'in_name' => 'Ing2',
                'in_measure_id' => 2,
                'in_supplier' => 'S2',
            ],
        ]);

    }
}
