<?php

use App\Recipe;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateRecipeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Recipe::where('r_name', 'R1')->exists()) {

            DB::table('recipe')->insert([
                [
                    'r_name' => 'R1',
                    'r_description' => 'D1',
                ],
                [
                    'r_name' => 'R2',
                    'r_description' => 'D2',
                ],
            ]);
        }

        DB::table('recipe_ingredients')->insert([
            [
                'ri_ingredient_id' => 1,
                'ri_amount' => 4.5,
                'ri_recipe_id' => 1,
            ],
            [
                'ri_ingredient_id' => 2,
                'ri_amount' => 33,
                'ri_recipe_id' => 1,
            ],
            [
                'ri_ingredient_id' => 1,
                'ri_amount' => 4,
                'ri_recipe_id' => 2,
            ],
            [
                'ri_ingredient_id' => 2,
                'ri_amount' => 1,
                'ri_recipe_id' => 2,
            ],
        ]);

    }
}
