<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = '2021-02-15';
        $currentDate = Carbon::now();
        $availableDate = Carbon::now()->addDays(1);
        if ($date > $availableDate) {
            DB::table('order')->insert([
                [
                    'o_user_id' => 1,
                    'o_recipe_id' => 1,
                    'o_delivery_date' => $date,
                ],
                [
                    'o_user_id' => 1,
                    'o_recipe_id' => 2,
                    'o_delivery_date' => $date,
                ],
                [
                    'o_user_id' => 3,
                    'o_recipe_id' => 2,
                    'o_delivery_date' => $date,
                ],
            ]);
        }
    }
}
