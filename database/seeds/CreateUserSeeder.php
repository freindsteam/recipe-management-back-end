<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $token = random_bytes(16);
        $token = bin2hex($token);

        DB::table('user')->insert([
            [
                'u_email' => 'u2@com',
                'u_first_name' => 'User2',
                'u_second_name' => 're',
                'u_last_name' => 'juhj',
                'u_token' => $token,
            ],
        ]);
    }
}
