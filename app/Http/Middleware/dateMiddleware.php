<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
class dateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $date = $request->deliveryDate;
        $currentDate = Carbon::now();
        $availableDate = Carbon::now()->addDays(1);
        if ($date < $availableDate){
            return response()->json([
                "message" => "Delivery date have to be after two days minimum from order date",
                "stauts"=>false
              ], 401);
        }
        return $next($request);
    }
}
