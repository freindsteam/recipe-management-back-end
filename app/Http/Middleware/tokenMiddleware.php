<?php

namespace App\Http\Middleware;

use Closure;

class tokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('token');
        if ($token != 'e819de31b68e6b17'){
            return response()->json([
                "message" => "Authentication failed",
                "stauts"=>false
              ], 401);
        }
        return $next($request);
    }
}
