<?php

namespace App\Http\Controllers;

use App\Recipe;
use App\RecipeIngredients;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RecipeController extends Controller
{
    public function addNewRecipe(Request $request)
    {
        $recipe = new Recipe;
        $ingredient = new RecipeIngredients;
        $recipe->r_name = $request->recipeName;
        $recipe->r_description = $request->recipeDescription;
        $ingredientsItem = array();
        if (!Recipe::where('r_name', $recipe->r_name)->exists()) {

            $recipe->save();
            $allIngrediants = $request->ingredientsItem;

            for ($i = 0; $i < count($allIngrediants); $i++) {

                array_push($ingredientsItem, ['ri_recipe_id' => $recipe->r_id, 'ri_ingredient_id' => $allIngrediants[$i]["ingredientId"], 'ri_amount' => $allIngrediants[$i]["qty"]]);
            }

            if ($ingredient->insert($ingredientsItem)) {
                return response()->json([
                    "message" => "Recipe created successfully",
                    "status" => true,
                ], 200);
            } else {
                return response()->json([
                    "message" => "Recipe not created successfully",
                    "status" => false,
                ], 200);
            }

        } else {
            return response()->json([
                "message" => "Recipe is already exist",
                "stauts" => false,
            ], 202);
        }
    }

    public function getAllRecipes()
    {

        $recipe = DB::table('recipe_view')->paginate(2);
        return response()->json([
            "recipes" => $recipe,
            "status" => true,
        ], 200);
    }

    public function deleteRecipe(Request $request)
    {

        $recipeId = $request->recipeId;
        if (Recipe::where('r_id', $recipeId)->exists()) {
            RecipeIngredients::whereIn('ri_recipe_id', explode(",", $recipeId))->delete();
            $recipe = Recipe::find($recipeId);
            $recipe->delete();

            return response()->json([
                "message" => "Recipe deleted successfully",
                "status" => true,
            ], 200);
        } else {
            return response()->json([
                "message" => "Recipe not found",
                "stauts" => false,
            ], 202);
        }
    }

    public function updateRecipe(Request $request)
    {
        $ingredient = new RecipeIngredients;

        $recipe = Recipe::find($request->recipeId);
        $recipe->r_name = $request->recipeName;
        $recipe->r_description = $request->recipeDescription;
        $result = $recipe->save();

        if ($result) {
            $newGradients = $request->recipeIngredients;

            RecipeIngredients::where('ri_recipe_id', '=', $request->recipeId)->delete();
            $newIngredientsItemsArray = array();
            for ($i = 0; $i < count($newGradients); $i++) {
                array_push($newIngredientsItemsArray, ['ri_recipe_id' => $request->recipeId, 'ri_ingredient_id' => $newGradients[$i]["ingredientId"], 'ri_amount' => $newGradients[$i]["qty"]]);
            }
            $ingredient->insert($newIngredientsItemsArray);

            return response()->json([
                "message" => "Recipe info updated successfully",
                "status" => true,
            ], 200);
        } else {
            return response()->json([
                "message" => "Recipe info updating failed",
                "stauts" => false,
            ], 202);
        }
    }

    public function getRecipeById($recipeId)
    {

        if (Recipe::where('r_id', $recipeId)->exists()) {
            $recipeBasicInfo = DB::table('recipe_view')->where('r_id', $recipeId)->first();
            $recipeGradients = DB::table('recipe_ingredients_view')->where('ri_recipe_id', $recipeId)->get();
            return response()->json([
                "recipeBasicInfo" => $recipeBasicInfo,
                "recipeIngrediants" => $recipeGradients,
                "status" => true,
            ], 200);
        } else {
            return response()->json([
                "message" => "Recipe not found",
                "status" => false,
            ], 202);
        }
    }

}
