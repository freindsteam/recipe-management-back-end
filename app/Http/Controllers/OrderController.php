<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function addNewOrder(Request $request)
    {
        $order = new Order;
        $order->o_user_id = $request->userId;
        $order->o_recipe_id = $request->recipeId;
        $order->o_delivery_date = $request->deliveryDate;

        $order->save();

        return response()->json([
            "message" => $order,
            "status" => true,
        ], 200);

    }

    public function getAllOrdersInfo()
    {

        $order = DB::table('order_info_view')->paginate(4);
        return response()->json([
            "recipes" => $order,
            "status" => true,
        ], 200);
    }

}
