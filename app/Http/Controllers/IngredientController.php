<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ingredient;
use Illuminate\Support\Facades\DB;

class IngredientController extends Controller
{

    public function addNewIngredient(Request $request) {
        $ingredient = new Ingredient;
        if (!Ingredient::where('in_name', $request->ingredientName)->exists()){
        $ingredient->in_name = $request->ingredientName;
        $ingredient->in_measure = $request->ingredientMeasure;
        $ingredient->in_supplier = $request->ingredientSupplier;
      
        $ingredient->save();
      
        return response()->json([
          "ingredient" => $ingredient,
          "status" => true
        ], 200);
      }
      else {
        return response()->json([
          "message" => "Ingredient already exist",
          "stauts" => false
        ], 202);
      }
      }

      public function getAllIngredients() {
        $ingredient = DB::table('ingredient_view')->paginate(2);
        return response()->json([
          "ingredients" => $ingredient,
          "status" => true
        ], 200);
      }
  

}
