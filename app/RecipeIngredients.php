<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeIngredients extends Model
{
    public $timestamps = false;
    protected $table = 'recipe_ingredients';
    protected $primaryKey = 'ri_id';
}
